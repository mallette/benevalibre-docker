FROM python:3.10.9-alpine3.17

RUN apk add --no-cache supervisor nginx uwsgi-python3 libmagic py3-mysqlclient py3-psycopg2 libjpeg-turbo-dev zlib-dev git

RUN mkdir -p /var/www/app \
  && chown -R nobody.nobody /var/www/app \
  && chown -R nobody.nobody /run \
  && chown -R nobody.nobody /var/lib/nginx \
  && chown -R nobody.nobody /var/log/nginx \
  && touch /var/www/app/config.env

COPY --chown=nobody:nobody entrypoint.sh /
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/uwsgi.ini /etc/uwsgi/uwsgi.ini
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

WORKDIR /var/www/app

ENV ENV=production
ARG BENEVALIBRE_VERSION=v1.4.0

RUN apk add --no-cache --virtual .build-deps curl make gcc musl-dev \
  && curl -L https://forge.cliss21.org/cliss21/benevalibre/archive/${BENEVALIBRE_VERSION}.tar.gz | tar -xz --strip-components=1 \
  && make DJANGO_DATABASE_URL=dummy DJANGO_SECRET_KEY=dummy DEFAULT_FROM_EMAIL=dummy USE_VENV=n install-deps static \
  && chown -R nobody.nobody /var/www/app/var \
  && apk --purge del .build-deps

ENV UWSGI_PLUGIN=python3
ENV PYTHONPATH=/usr/local/lib/python3.10/site-packages:/usr/lib/python3.10/site-packages

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
