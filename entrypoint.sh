#!/bin/sh

chown -R nobody.nobody /var/www/app/var/media
su -s /bin/sh -c "python3 manage.py migrate" nobody

exec "$@"
