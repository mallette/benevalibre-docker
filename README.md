# Docker image for Benevalibre

[Bénévalibre](https://benevalibre.org/) is a free software which ease volunteering management and appreciation in organizations.

## Quick start

Run `docker run --rm  -p 80:80 -e "DJANGO_EMAIL_URL=consolemail://" -e DJANGO_ALLOWED_HOSTS=localhost -e DJANGO_SECRET_KEY=changeme -e DEFAULT_FROM_EMAIL=email@domain.tld -e DJANGO_DATABASE_URL=sqlite:///var/sqlite.db ghcr.io/colibris-xyz/benevalibre`  
Then visit http://localhost in your web browser.

## Configuration

### Using environment variables

`docker run -p 80:80 --env-file config.env ghcr.io/colibris-xyz/benevalibre`

### Mounting a config.env file

`docker run -p 80:80 -v /path/to/config.env:/var/www/app/config.env ghcr.io/colibris-xyz/benevalibre`

## Persistent data

Mount the `/var/www/app/var/media` path into a volume to persist user-uploaded media:

`docker run -v media:/var/www/app/var/media ghcr.io/colibris-xyz/benevalibre`

## Running with docker compose

Copy `.env.example` to `.env` and adapt it to your needs. Then :
```
docker compose up -d
```

## Managing benevalibre

```
docker compose exec  benevalibre /var/www/app/manage.py help
```
You probably want to run :
```
docker compose exec  benevalibre /var/www/app/manage.py createsuperuser
```
